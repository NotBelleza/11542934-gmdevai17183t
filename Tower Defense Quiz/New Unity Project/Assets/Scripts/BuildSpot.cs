﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildSpot : MonoBehaviour {

    public GameObject towerPrefab;
    public GameObject towerPrefab1;
    void OnMouseUpAsButton()
    {
        GameObject g;
        // Build Tower above Buildplace
        if (Input.GetButton("Fire1"))
        {
            g = (GameObject)Instantiate(towerPrefab1);


        }

        else
            g = (GameObject)Instantiate(towerPrefab);

        g.transform.position = transform.position + Vector3.up;
    }

}
