﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    TextMesh tMesh;

    // Use this for initialization
    void Start () {
        tMesh = GetComponent<TextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
        transform.forward = Camera.main.transform.forward;

    }

    // Return the current Health by counting the '-'
    public int Current()
    {
        return tMesh.text.Length;
    }

    // Decrease the current Health by removing one '-'
    public void Decrease()
    {
        if (Current() > 1)
            tMesh.text = tMesh.text.Remove(tMesh.text.Length - 1);
        else
        {

            Destroy(transform.parent.gameObject);
            Destroy(gameObject);
        }
    }
}
