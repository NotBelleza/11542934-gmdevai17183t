﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour {

    [SerializeField]
    public float timeBetweenWaves = 5f;
    private float countdown = 2f;
    private int waveNumber = 1;

    public GameObject enemyPrefab;
    public Node spawn;
    public Node end;

    public Graph path;

    public float spawnInterval = 0.5f;

    void Update()
    {
        if (countdown <= 0.0f)
        {
            StartCoroutine(SpawnWave());
            countdown = timeBetweenWaves;

        }

        countdown -= Time.deltaTime;
    }

    // Use this for initialization
    void Start ()
    {

    }
	
	// Update is called once per frame
	IEnumerator SpawnWave () {
        for(int i = 0; i < waveNumber; i++)
        {
            SpawnEnemy();
            yield return new WaitForSeconds(spawnInterval);
        }

        waveNumber++;

    }

    void SpawnEnemy()
    {
        
        GameObject enemy = (GameObject) Instantiate(enemyPrefab, transform.position, Quaternion.identity);
        Follower enemyFollower = enemy.GetComponent<Follower>();
        enemyFollower.m_Start = spawn;
        enemyFollower.m_End = end;
        enemyFollower.m_Graph = path;
    }
}
