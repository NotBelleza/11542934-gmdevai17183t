﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter(Collider co)
    {
        // If castle then deal Damage
        if (co.name == "EndPoint")
        {
            co.GetComponentInChildren<Health>().Decrease();
            Destroy(gameObject);
        }
    }
    
}
