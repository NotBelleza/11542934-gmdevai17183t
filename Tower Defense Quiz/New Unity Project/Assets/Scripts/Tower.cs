﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {

    public float fireRate = 1f;
    private float fireCountdown = 0f;

    public GameObject bulletPrefab;
    // Update is called once per frame
    void Update () {

    }


    private void OnTriggerStay(Collider co)
    {
        if (fireCountdown <= 0.0f)
        {
            Fire(co);
            fireCountdown = 1f / fireRate;

        }

        fireCountdown -= Time.deltaTime;
    }

    void Fire(Collider co)
    {
        GameObject g = (GameObject)Instantiate(bulletPrefab, new Vector4(transform.position.x, transform.position.y + 0.1f, transform.position.z), Quaternion.identity);
        if (g.GetComponent<Bullet>() != null)
            g.GetComponent<Bullet>().target = co.transform;
        if (g.GetComponent<HeavyBullet>() != null)
            g.GetComponent<HeavyBullet>().target = co.transform;
        Physics.IgnoreCollision(g.GetComponent<Collider>(), GetComponent<Collider>());
    }
}

